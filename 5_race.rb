### wait2 will reap dead child status as long as the parent 
### process is running
2.times do
  fork do
    # Both processes exit immediately.
    puts "Running #{Process.pid}, parent: #{Process.ppid}"
    abort "Finished!"
  end
end

puts Process.wait2
# making sure the 2nd child process has exited while we sleep
sleep 5

puts Process.wait2
