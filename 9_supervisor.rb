### The correct way to implement the Supervisor pattern

child_processes = 3
dead_processes = 0

child_processes.times do
  fork { sleep 3}
end

# Sync $stdout so the call to #puts in the CHLD handler isn't
# buffered. Can cause a ThreadError if a signal handler is
# interrupted after calling #puts. Always a good idea to do
# this if your handlers will be doing IO.
$stdout.sync = true

# The :CHLD signal will be fired when a child dies
trap(:CHLD) do
  begin
    # one CHLD signal may be fired for more than 1 dead 
    # children, so we wait in a loop
    while pid = Process.wait2(-1, Process::WNOHANG)
      p pid
      dead_processes += 1      
    end
  rescue Errno::ECHILD
  end
end

loop do
  # We exit the supervisor once all the child processes are accounted for.
  exit if dead_processes == child_processes
end