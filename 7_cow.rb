### copy-on-write semantics
require 'get_process_mem'

arr = []
10_000_000.times { |i| arr << i }
puts "Parent process #{Process.pid} uses #{GetProcessMem.new.mb} MB"

fork do
	arr << 45454
  p arr.last
  puts "Child process #{Process.pid} uses #{GetProcessMem.new.mb} MB"
end

sleep 2
p arr.last
