### Ruby makes it easy with blocks

fork do
  # this runs in the child process
  puts "This is child process #{Process.pid} of parent #{Process.ppid}"
  sleep 2
end

# this runs in the parent process
puts "This is parent process #{Process.pid}"
