### Failure to reap dead child process while parent is
### running, makes dead process a zombie
2.times do
  fork do
    # Both processes exit immediately.
    puts "Running #{Process.pid}, parent: #{Process.ppid}"
    abort "Finished!"
  end
end

puts Process.wait2
sleep 30

# puts Process.wait2