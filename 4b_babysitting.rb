# We create 5 child processes.
4.times do
  fork { exit (rand(5).even?) ? 1 : 2 }
end


loop do
  # We wait for each of the child processes to exit.
  begin
    pid, status = Process.wait2

    # If the child process exited with the 1 exit code
    # then we know they encountered an even number.
    if status.exitstatus == 1
      puts "#{pid} encountered an even number!"
    else
      puts "#{pid} encountered an odd number!"
    end

  # if we call wait2 when no more child processes are dead
  # it will raise an ECHILD exception
  rescue Errno::ECHILD
    puts "no more dead processes"
    exit
  end
end
