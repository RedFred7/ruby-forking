# We create 5 child processes.
4.times do
  fork { exit (rand(5).even?) ? 1 : 2 }
end


5.times do
  # We wait for each of the child processes to exit.
  pid, status = Process.wait2

  # If the child process exited with the 1 exit code
  # then we know they encountered an even number.
  if status.exitstatus == 1
    puts "#{pid} encountered an even number!"
  else
    puts "#{pid} encountered an odd number!"
  end
end